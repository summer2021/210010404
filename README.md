# Summer2021-No.76 开发IO热度统计的可视化工具

### 题目介绍
https://gitee.com/openeuler-competition/summer-2021/issues/I3GX7M

### 软件说明
该工具基于eBPF和urwid开发，主要用于监视磁盘扇区和文件目录的访问频率，可以设置监控的磁盘和路径。  
按 `F4` 键进入设置模式，按 `F5` 键退出程序。  

<img src="figure.png" width = "500" height = "300" alt="figure" align=center />  

### 环境配置

##### bcc库安装  

参考bcc官方提供的安装方式 [https://github.com/iovisor/bcc/blob/master/INSTALL.md](https://github.com/iovisor/bcc/blob/master/INSTALL.md)  


```bash
sudo dnf install -y bison cmake ethtool flex git iperf libstdc++-static \
python-netaddr python-pip gcc gcc-c++ make zlib-devel \
elfutils-libelf-devel python-cachetools
sudo dnf install -y luajit luajit-devel  
sudo dnf install -y http://repo.iovisor.org/yum/extra/mageia/cauldron/x86_64/netperf-2.7.0-1.mga6.x86_64.rpm
sudo pip install pyroute2

sudo dnf install -y clang clang-devel llvm llvm-devel llvm-static ncurses-devel
sudo dnf install elfutils-libelf-devel
sudo dnf install cmake

git clone https://github.com/iovisor/bcc.git
mkdir bcc/build; cd bcc/build
cmake ..
make
sudo make install
```
##### urwid库安装  

直接用pip3安装即可  `pip3 install urwid`


#### 使用说明

运行 `python3 ioheat.py` 即可使用工具
