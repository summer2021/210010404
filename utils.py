import os


##### Disk Info #####
def get_disk_info():
    fdisk_parse = os.popen("fdisk -l").readlines()
    disk_info = []
    for l in fdisk_parse:
        if l.startswith("Disk /dev/sd"):
            d = {}
            ls = l.split()
            d["name"] = ls[1][5:-1] # Disk Name
            d["sector"] = ls[6]     # Total Sector
            disk_info.append(d)
    return disk_info

def parse_sector(list):
    res = [0] * 10
    for i, j in list:
        res[i] = j
    return res


##### Palette Info #####
palette = [('level0', '', '', '', '', ''),
            ('level1', '', '', '', '', '#600'),
            ('level2', '', '', '', '', '#800'),
            ('level3', '', '', '', '', '#a00'),
            ('level4', '', '', '', '', '#d00'),
            ('level5', '', '', '', '', '#f00'),
            ('level6', '', '', '', '', '#f60'),
            ('level7', '', '', '', '', '#f80'),
            ('level8', '', '', '', '', '#fa0'),
            ('level9', '', '', '', '', '#fd0'),
            ('level10', '', '', '', '', '#ff0'),
            ('level11', '', '', '', '', '#ff6'),
            ('level12', '', '', '', '', '#ffa'),
            ('level13', '', '', '', '', '#ffd'),
            ('level14', '', '', '', '', '#fff'),
            ('bg', 'black', 'dark blue'),
            ('header', 'black,underline', 'light gray', 'standout,underline', 'black,underline', '#88a'),
            ('panel', 'light gray', 'dark blue', '', '#ffd', '#00a'),
            ('focus', 'light gray', 'dark cyan', 'standout', '#ff8', '#806'),
           ]