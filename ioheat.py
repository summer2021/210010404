from bcc import BPF
import urwid
from time import sleep
import os
from utils import get_disk_info, palette
import math

#### BPF Init ####
HIST_REG = ""
UPDATE_SEC = ""
UPDATE = """
if(##CONDITION##){
sect_key_t ##VAR## = {.sector = data.sector / (##TOTAL_SECTOR## / 10) };
dist_##NAME##.atomic_increment(##VAR##);
}
"""

disk_info = get_disk_info()

with open("ioheat.c", "r") as f:
    bpf_text = f.read()

for d in disk_info:
    name = d["name"]
    HIST_REG += f"BPF_HISTOGRAM(dist_{name}, sect_key_t);"
    string = []
    for i, c in enumerate(name):
        string.append(f" data.disk_name[{i}] == '{c}' ")
    string = "&&".join(string)

    temp_str = UPDATE.replace("##CONDITION##", string)
    temp_str = temp_str.replace("##VAR##", "key_"+name)
    temp_str = temp_str.replace("##TOTAL_SECTOR##", d["sector"])
    temp_str = temp_str.replace("##NAME##", name)

    UPDATE_SEC += temp_str

bpf_text = bpf_text.replace("##UPDATE_SEC##", UPDATE_SEC).replace("##HIST_REG##", HIST_REG)

b = BPF(text='')
fnname_open = b.get_syscall_prefix().decode() + 'open'
fnname_openat = b.get_syscall_prefix().decode() + 'openat'
fnname_openat2 = b.get_syscall_prefix().decode() + 'openat2'
if b.ksymname(fnname_openat2) == -1:
    fnname_openat2 = None

b = BPF(text=bpf_text)
b.attach_kprobe(event="blk_account_io_start", fn_name="trace_pid_start")
if BPF.get_kprobe_functions(b'blk_start_request'):
    b.attach_kprobe(event="blk_start_request", fn_name="trace_req_start")
b.attach_kprobe(event="blk_mq_start_request", fn_name="trace_req_start")
b.attach_kprobe(event="blk_account_io_done", fn_name="trace_req_completion")

b.attach_kprobe(event=fnname_open, fn_name="syscall__trace_entry_open")
b.attach_kretprobe(event=fnname_open, fn_name="trace_return")

b.attach_kprobe(event=fnname_openat, fn_name="syscall__trace_entry_openat")
b.attach_kretprobe(event=fnname_openat, fn_name="trace_return")

b.attach_kprobe(event=fnname_openat2, fn_name="syscall__trace_entry_openat2")
b.attach_kretprobe(event=fnname_openat2, fn_name="trace_return")


#### TUI Init ####

class MainWindow(urwid.Widget):

    palette = [
            # ('level0', '', '', '', '', '#600'),
            # ('level1', '', '', '', '', '#800'),
            # ('level2', '', '', '', '', '#a00'),
            # ('level3', '', '', '', '', '#d00'),
            # ('level4', '', '', '', '', '#f00'),
            # ('level5', '', '', '', '', '#f60'),
            # ('level6', '', '', '', '', '#f80'),
            # ('level7', '', '', '', '', '#fa0'),
            # ('level8', '', '', '', '', '#fd0'),
            # ('level9', '', '', '', '', '#ff0'),
            ('level0', '', '', '', '', '#000'),
            ('level1', '', '', '', '', '#600'),
            ('level2', '', '', '', '', '#800'),
            ('level3', '', '', '', '', '#a00'),
            ('level4', '', '', '', '', '#d00'),
            ('level5', '', '', '', '', '#f00'),
            ('level6', '', '', '', '', '#f60'),
            ('level7', '', '', '', '', '#f80'),
            ('level8', '', '', '', '', '#fa0'),
            ('level9', '', '', '', '', '#fd0'),
            ('level10', '', '', '', '', '#ff0'),
            ('level11', '', '', '', '', '#ff6'),
            ('level12', '', '', '', '', '#ffa'),
            ('level13', '', '', '', '', '#ffd'),
            ('level14', '', '', '', '', '#fff'),
            ('bg', 'black', 'dark blue'),
            ('header', 'black,underline', 'light gray', 'standout,underline', 'black,underline', '#88a'),
            ('panel', 'light gray', 'dark blue', '', '#ffd', '#00a'),
            ('focus', 'light gray', 'dark cyan', 'standout', '#ff8', '#806'),
            ]

    def __init__(self, bpf):

        self.bpf =bpf
        self.colums = urwid.Columns
        self.rows = urwid.Pile
        
        self.sec_heat_init()
        
        self.screen = urwid.raw_display.Screen()
        self.screen.register_palette(palette)
        self.screen.set_terminal_properties(colors=2**24)

        self.disk_info = get_disk_info()

        self.div = urwid.Divider()

        self.init_ptree()
        self.controller_init()
        

        self.bottom = urwid.Columns([("weight", 0.2, self.controller), ("weight", 0.8, self.path_heatmap)]) 
        self.top = urwid.LineBox(self.sec_heatmap, title="Sector Heatmap",title_align="left", title_attr="header")

        self.main_box = urwid.ListBox([self.top, urwid.Divider(), self.bottom])

        self.main_loop = urwid.MainLoop(self.main_box, screen=self.screen, unhandled_input=self.exit_on_q)
        self.alarm = self.main_loop.set_alarm_in(1, self.alarm_func, user_data=None)
        # self.main_box.focus_position = 0
        self.main_loop.run()
    
    #### Sector HeatMap####
    def sec_heat_init(self):
        self.sec_temp = []
        for i in range(40):
            temp_list = []
            for j in range(10):
                temp_list.append(self.LevelGrid(0))
            temp_pile = self.rows(temp_list)
            self.sec_temp.append(temp_pile)
        self.sec_heatmap = self.colums(self.sec_temp)

    def sec_heat_update(self, list):
        self.sec_temp.pop(0)
        temp_list = []
        for i in list:
           temp_list.append(self.LevelGrid(i))
        temp_pile = self.rows(temp_list)
        self.sec_temp.append(temp_pile)
        self.sec_heatmap = self.colums(self.sec_temp)

    def LevelGrid(self, level):
        return urwid.AttrMap(urwid.Text(" "), 'level'+str(math.floor(math.log(level+1,2))))

    def parse_sector(self, list):
        res = [0] * 10
        for i, j in list:
            res[i] = j
        return res
    
    #### Path Heatmap ####
    def build_ptree(self, rootpath):
        self.path_dict = {}
        file_list = os.listdir(rootpath)
        temp = {}
        indent = "|-- "
        for f in file_list:
            temp["urwid_fmt"] = '{}{}'.format(indent, f)
            temp["cnt"] = 0
            self.path_dict[os.path.join(rootpath, f)] = temp.copy()

    def init_ptree(self):
        self.rootpath = "/"
        self.build_ptree(self.rootpath)
        self.urwid_dir_list = [urwid.AttrMap(urwid.Text(self.rootpath), 'level0')]
        for k,v in self.path_dict.items():
            self.urwid_dir_list.append(urwid.AttrMap(urwid.Text(v["urwid_fmt"]), 'level'+str(math.floor(math.log(v["cnt"]+1,2)))))
        self.path_heatmap = urwid.LineBox(urwid.Pile(self.urwid_dir_list), title="Path Heatmap",title_align="left", title_attr="header")

    def update_ptree(self, list):
        for k, v in self.path_dict.items():
            v["cnt"] =0
        for path in list:
            for k, v in self.path_dict.items():
                if k in path:
                    v["cnt"] += 1
        self.urwid_dir_list = [urwid.AttrMap(urwid.Text(self.rootpath), 'level0')]
        for k,v in self.path_dict.items():
            self.urwid_dir_list.append(urwid.AttrMap(urwid.Text(v["urwid_fmt"]), 'level'+str(math.floor(math.log(v["cnt"]+1,2)))))
        self.path_heatmap = urwid.LineBox(urwid.Pile(self.urwid_dir_list), title="Path Heatmap",title_align="left", title_attr="header")


    #### Controller  ####
    def controller_init(self):
        ### Disk ###
        self.disk_bottom_init()
        self.path_set_init()
        ### Path ###
        self.controller_widget = self.rows([self.sector_prompt, self.buttom_wedget, self.path_prompt, self.path_set, self.path_reply, self.path_button])
        self.controller = urwid.LineBox(self.controller_widget, title="Controller",title_align="left", title_attr="header")
    
    def path_set_init(self):
        self.path_prompt = urwid.Text("Set Path:")
        self.path_set = urwid.Edit(("panel", u""))
        self.path_reply = urwid.Text(u"Path: %s"%self.rootpath)
        self.path_button = urwid.Button(u"Enter")
        urwid.connect_signal(self.path_button, 'click', self.path_change)
        urwid.connect_signal(self.path_set, 'postchange', self.path_change)

    def path_change(self, button):
        path = self.path_set.get_edit_text()
        if path == self.rootpath:
            self.path_reply.set_text(u"Path not change.")
        elif os.path.exists(path):
            self.rootpath = path
            self.path_reply.set_text(u"Path: %s"%self.rootpath)
            self.build_ptree(self.rootpath)
            self.urwid_dir_list = [urwid.AttrMap(urwid.Text(self.rootpath), 'level0')]
            for k,v in self.path_dict.items():
                self.urwid_dir_list.append(urwid.AttrMap(urwid.Text(v["urwid_fmt"]), 'level'+str(math.log(v["cnt"]+1,2))))
            self.path_heatmap = urwid.LineBox(urwid.Pile(self.urwid_dir_list), title="Path Heatmap",title_align="left", title_attr="header")
            self.bottom = urwid.Columns([("weight", 0.2, self.controller), ("weight", 0.8, self.path_heatmap)]) 
            self.main_box.body = [self.top, urwid.Divider(), self.bottom]
        else:
            self.path_reply.set_text(u"Not find %s."%path)
        self.alarm = self.main_loop.set_alarm_in(1, self.alarm_func, user_data=None)

    def disk_bottom_init(self):
        self.sector_prompt = urwid.Text("Disk Selection:")
        self.disk_radio_buttom = []
        self.focus_disk = self.disk_info[0]["name"]
        self.buttom_list = []
        for d in self.disk_info:
            self.buttom_list.append(self.disk_rb(d["name"], d["name"]))
        self.buttom_wedget = urwid.AttrMap(urwid.Pile(self.buttom_list), "body")

    def disk_rb(self, text, disk_name, state=False):
        rb = urwid.RadioButton(self.disk_radio_buttom, text, state)
        urwid.connect_signal(rb, 'change', self.switch_disk, disk_name)
        rb = urwid.AttrMap(rb, None, 'focus')
        return rb
    
    def on_ask_change(self, edit, new_edit_text):
        self.path_change(new_edit_text)

    def switch_disk(self, rb, state, disk_name):
        if state:
            self.focus_disk = disk_name
            self.sec_heat_init()
            self.alarm = self.main_loop.set_alarm_in(1, self.alarm_func, user_data=None)

    #### BPF Update ####
    def alarm_func(self, loop, data):
        temp_focus1 = self.main_box.focus_position
        temp_focus2 = self.bottom.focus_position
        self.alarm = self.main_loop.set_alarm_in(1, self.alarm_func, user_data=None)
        sector_data = [[k[0].sector, k[1].value] for k in self.bpf["dist_" + self.focus_disk].items()]
        sector_data = self.parse_sector(sector_data)
        self.bpf["dist_" + self.focus_disk].clear()
        self.sec_heat_update(sector_data)
        self.top = urwid.LineBox(self.sec_heatmap, title="Sector Heatmap",title_align="left", title_attr="header")
        ### filename
        file_list = []
        for k, v in self.bpf["datax"].items():
            file_list.append(k.fname.decode('utf-8', 'replace'))
        self.bpf["datax"].clear()
        self.update_ptree(file_list)
        self.bottom = urwid.Columns([("weight", 0.2, self.controller), ("weight", 0.8, self.path_heatmap)]) 
        self.main_box.body=[self.top, self.div, self.bottom]
        self.main_box.focus_position = temp_focus1
        self.bottom.focus_position = temp_focus2

    def exit_on_q(self, key):
        if key == "f5":
            raise urwid.ExitMainLoop()
        elif key == "f4":
            self.main_loop.remove_alarm(self.alarm)

    class ChangeEdit(urwid.Edit):
        def keypress(self, size, key):
            if key != "enter":
                return super().keypress(size, key)
            

def main():
    mainwindow = MainWindow(b)

if '__main__'==__name__:
    main()